from typing import Any, Mapping, Sequence
import os
import re
import json
from dataclasses import dataclass

JSON_PATH = os.path.join(os.path.dirname(__file__), "actors.jsonc")

actors = {
	# "collection": Sequence[Actor]
}

# =)
@dataclass
class Actor:
	name: str
	icon: str
	visible: bool
	etype: str
	extras: None | Mapping[str, Sequence[Any]]
# extras format: {name: [input type, arg0, arg1, ···]}
# example: ["movie-pos", 0.0, 1.0, 1.0] (movie-pos)
# special types include movie pos, dropdown

# Reads the actors json *once* and then is accessed by everything else
def load_actors():
	try:
		with open(JSON_PATH, "r") as f:
			actors.clear()

			# Get rid of comments =)
			json_string = f.read()
			new_string = re.sub(r"[^\\]//.+", "", json_string) # not very good but works for now

			json_data = json.loads(new_string)

			for collection in json_data:
				for actor_data in json_data[collection]:
					new_actor = Actor(
						name=actor_data.get("Name", "Unknown"),
						icon=actor_data.get("Icon", "Unknown"),
						visible=True,
						etype=actor_data.get("Actor Type", "money"),
						extras=actor_data.get("Properties"),
					)

					if collection not in actors.keys():
						actors[collection] = []
					actors[collection].append(new_actor)
					
	except Exception as e:
		print(f"Can't read file: {JSON_PATH}")
		print(e)

load_actors()