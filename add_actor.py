##############################################################################
# Imports           Order: 3rd Party Imports, Python Built-ins, Custom Modules
##############################################################################


import bpy
import os
import json
import bpy.utils.previews

from .actors import actors

##############################################################################
# Classes
##############################################################################


classes = []  # Initialize the class array to be registered


# name following the convention of existing menus
class VIEW3D_MT_actor_add(bpy.types.Menu):
	"""The "add actor" menu"""

	bl_idname = "VIEW3D_MT_actor_add"
	bl_label = "Actor"

	def draw(self, context):

		pass


classes.append(VIEW3D_MT_actor_add)  # Add the class to the array


class ActorSpawnButton(bpy.types.Operator):
	"""Creates an actor mesh"""  # Be careful, operator docstrings are exposed to the user

	bl_idname = "object.spawn_actor"  # Unique operator reference name
	bl_label = "Actor"  # String for the UI
	bl_options = {"REGISTER", "UNDO"}  # Enable undo for the operator

	actor_name: bpy.props.StringProperty()
	mesh_name: bpy.props.StringProperty()
	icon_name: bpy.props.StringProperty()  # The name of the icon on this button
	cat: bpy.props.StringProperty()  # The category in which to find all the actor data in the json file
	idx: bpy.props.IntProperty()  # The index in that category to find all the actor data in the json file

	def execute(cls, context):  # execute() is called when running the operator
		scene = context.scene

		actor = bpy.ops.object.empty_add()

		# Set its rotation to quaternions
		bpy.context.object.rotation_mode = "QUATERNION"

		bpy.context.object.location = bpy.data.scenes["Scene"].cursor.location

		# Create the actor collection if needed
		create_actor_collection()

		# Add to the collection
		bpy.ops.object.collection_link(collection="Actor Collection")

		this_obj = bpy.context.object
		copy_obj = actors[cls.cat][cls.idx]
		this_obj["Actor Type"] = copy_obj.etype

		level_nick = context.scene.level_properties.nickname
		prefix = f"{level_nick}-{copy_obj.etype}"

		# Let's count together!
		# In the name of UX
		obj_count = 0
		last_num = -1
		for obj in bpy.data.objects:
			if obj.name.startswith(prefix):
				current_num = obj.name[(len(prefix) + 1):]
				if current_num.isdecimal(): # Limited, but works here
					if int(current_num) != last_num + 1:
						# There's a gap in the numbers, pick first missing num
						break
					last_num = int(current_num)
				obj_count += 1

		this_obj.name = f"{prefix}-{obj_count}"
		this_obj["Icon"] = cls.icon_name
		this_obj["Category"] = cls.cat
		this_obj["Index"] = cls.idx
		
		# Retrieve last values if any
		this_obj.buzzer_props.copy(scene.last_buzzer_props)
		this_obj.cell_props.copy(scene.last_cell_props)
		this_obj.eco_props.copy(scene.last_eco_props)
		this_obj.vent_props.copy(scene.last_vent_props)
		this_obj.orb_cache_props.copy(scene.last_orb_cache_props)
		this_obj.crate_props.copy(scene.last_crate_props)

		# Do additional setup
		# cause UX
		match this_obj["Actor Type"]:
			case "crate":
				# TODO: Can do this better if text matches container type
				match cls.actor_name:
					case "Wooden Crate":
						this_obj.crate_props.container_type = "wooden"
					case "Steel Crate":
						this_obj.crate_props.container_type = "steel"
					case "Scout Fly Crate":
						this_obj.crate_props.container_type = "iron"
					case "Dark Eco Crate":
						this_obj.crate_props.container_type = "darkeco"
					case "Barrel":
						this_obj.crate_props.container_type = "barrel"
					case "Bucket":
						this_obj.crate_props.container_type = "bucket"

		return {"FINISHED"}  # Let Blender know the operator finished successfully


classes.append(ActorSpawnButton)  # Add the class to the array


##############################################################################
# Functions
##############################################################################


custom_icons = bpy.utils.previews.new()

def add_custom_icons():
	"""Add the necessary custom icons"""

	# Path to the folder where the icon is
	# The path is calculated relative to this py file inside the addon folder
	my_icons_dir = os.path.join(os.path.dirname(__file__), "icons")

	# Load a preview thumbnail of a file and store in the previews collection
	for filename in os.listdir(my_icons_dir):
		custom_icons.load(
			filename.split(".")[0], os.path.join(my_icons_dir, filename), "IMAGE"
		)

def create_actor_collection():
	"""Creates actor_collection"""

	# Check if there is already a collection of actors
	if "Actor Collection" in bpy.data.collections:
		return 0

	# Create a collection to house the actors
	actor_collection = bpy.data.collections.new("Actor Collection")

	#  Add it as a child of the scene collection
	bpy.context.scene.collection.children.link(actor_collection)

def draw_actor_menu(self, context):
	"""Draws the "add actor" menu"""

	self.layout.menu(
		VIEW3D_MT_actor_add.bl_idname,
		icon_value=custom_icons["open-goal"].icon_id,
	)

def draw_buttons(self, context):
	"""Draws the buttons within the "add actor" menu"""

	# Create a label in the "add actor" menu
	def label(txt, icn="open-goal", visible=True):
		if visible:
			self.layout.label(text=txt, icon_value=custom_icons[icn].icon_id)

	# Create a button in the "add actor" menu
	def button(txt, icn, mesh, visible, cat, idx):
		if visible:
			button = self.layout.operator(
				ActorSpawnButton.bl_idname,
				text=txt,
				icon_value=custom_icons[icn].icon_id,
			)
			button.actor_name = txt
			button.mesh_name = mesh
			button.icon_name = icn
			button.cat = cat
			button.idx = idx
	
	# New way of loading stuffs
	for (i, collection) in enumerate(actors):
		label(collection, icn="invisible")
		for (idx, actor) in enumerate(actors[collection]):
			button(actor.name, actor.icon, "Suzanne", actor.visible, collection, idx)
		if i < len(actors) - 1:
			label("", icn="invisible")


##############################################################################
# Registration
##############################################################################


def register():

	for cls in classes:  # Register all the classes
		bpy.utils.register_class(cls)

	# Add the custom icons
	add_custom_icons()

	# Add the "add actor" menu
	bpy.types.VIEW3D_MT_add.prepend(draw_actor_menu)

	# Add buttons to the "add actor" menu
	bpy.types.VIEW3D_MT_actor_add.append(draw_buttons)


def unregister():

	for cls in reversed(classes):  # Unregister all the classes
		bpy.utils.unregister_class(cls)

	# Remove the "add actor" menu
	bpy.types.VIEW3D_MT_add.remove(draw_actor_menu)


# if __name__ == "__main__":  # For internal Blender script testing
#    register()
