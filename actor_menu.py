##############################################################################
# Imports           Order: 3rd Party Imports, Python Built-ins, Custom Modules
##############################################################################


import bpy


##############################################################################
# Classes
##############################################################################

# Stuffs
containers = [
	("wooden", "Wooden", "Wooden Crate"),
	("steel", "Steel", "Steel Crate"),
	("iron", "Iron", "Scout Fly Crate"),
	("darkeco", "Dark Eco", "Dark Eco Crate"),
	("barrel", "Barrel", ""),
	("bucket", "Bucket", "")]

containable = [
	("none", "Nothing", ""),
	("eco", "Eco", ""),
	("money", "Precursor Orbs", ""),
	("buzzer", "Scout Fly", ""),
	("fuel_cell", "Power Cell", "")]

eco_type = [
	("health_dot", "Health Dot", "A single green eco", 7),
	("eco_green", "Green Eco", "", 4),
	("eco_blue", "Blue Eco", "", 3),
	("eco_yellow", "Yellow Eco", "", 1),
	("eco_red", "Red Eco", "", 2)]

levels = [
	("training", "Geyser Rock", ""),
	("village1", "Sandover Village", ""),
	("beach", "Sentinel Beach", ""),
	("jungle", "Forbidden Jungle", ""),
	("jungleb", "Forbidden Temple", ""),
	("misty", "Misty Island", ""),
	("firecanyon", "Fire Canyon", ""),
	("village2", "Rock Village", ""),
	# TODO: More descriptive split level descriptions
	("sunken", "Lost Precursor City A", "Lost Precursor City – First Half"),
	("sunkenb", "Lost Precursor City B", "Lost Precursor City – Second Half"),
	("swamp", "Boggy Swamp", "Soggy Bwamp"), # :3
	("rolling", "Precursor Basin", ""),
	("ogre", "Mountain Pass", ""),
	("village3", "Volcanic Crater", ""),
	("snow", "Snowy Mountain", ""),
	("maincave", "Spider Cave (Main)", "Spider Cave – Main Part"),
	("darkcave", "Spider Cave (Dark)", "Spider Cave – Dark Cave"),
	("robocave", "Spider Cave (Robo)", "Spider Cave – Robo Cave"),
	("lavatube", "Lava Tube", ""),
	("citadel", "Gol and Maia's Citadel", "Gol and Maia's Citbadel")]

load_boundary_cmds = [
	("load", "Load Level", "Loads a level", 1),
	("display", "Display Level", "Displays a level", 3),
	("checkpt", "Checkpoint", "Assigns a checkpoint", 6),
	("vis", "Load Vis Data", "Loads vis data for level", 4),
	("force-vis", "Force Load Vis Data", "Forcefully loads vis data for level", 5)]

# Custom Properties

class MoviePosProperty(bpy.types.PropertyGroup):
	pos: bpy.props.FloatVectorProperty(
		name="Movie Position",
		description="Position of the movie scene",
		step=1,
		precision=4,
		unit="LENGTH",
		subtype="XYZ")
	rot: bpy.props.FloatProperty(
		name="Movie Rotation",
		description="Rotation of the movie scene",
		unit="ROTATION",
		subtype="ANGLE")
	
	def copy(cls, copy_from):
		cls["pos"] = copy_from.pos
		cls["rot"] = copy_from.rot

class LauncherVectorProperty(bpy.types.PropertyGroup):
	pos: bpy.props.FloatVectorProperty(
		name="pos",
		description="",
		step=1,
		precision=4,
		unit="LENGTH")
	w: bpy.props.FloatProperty(
		name="seek-time scalar",
		description="Scales the seek-time of the launcher",
		step=1,
		min=0.0)
	
	def copy(cls, copy_from):
		cls["pos"] = copy_from.pos
		cls["w"] = copy_from.w

class EcoInfoForEcoProperty(bpy.types.PropertyGroup):
	def update(cls, context):
		context.scene.last_eco_props.copy(cls)
		context.scene.last_crate_props.eco_info_eco.copy(cls) # UX

	kind: bpy.props.EnumProperty(
		items=eco_type,
		name="Type",
		description="What kind of eco?",
		update=update)
	amount: bpy.props.IntProperty(
		name="Amount",
		description="How much eco?",
		min=1,default=1,
		update=update)
	
	def copy(cls, copy_from):
		# DO NOT QUESTION THIS
		cls["kind"] = copy_from.get("kind", "Health Dot")
		cls["amount"] = copy_from.amount

class EcoInfoForMoneyProperty(bpy.types.PropertyGroup):
	amount: bpy.props.IntProperty(
		name="Amount",
		description="How much money?",
		min=1)
	
	def copy(cls, copy_from):
		cls["amount"] = copy_from.amount

class EcoInfoForScoutFlyProperty(bpy.types.PropertyGroup):
	def update(cls, context):
		context.scene.last_buzzer_props.copy(cls)
		context.scene.last_cell_props.copy(cls) # UX
	
	game_task: bpy.props.IntProperty(
		name="Game Task",
		description="Which game task handles this scout fly?",
		min=0,
		update=update)
	movie_pos: bpy.props.PointerProperty(type=MoviePosProperty,update=update)
	
	def copy(cls, copy_from):
		cls["game_task"] = copy_from.game_task
		cls.movie_pos.copy(copy_from.movie_pos)

class EcoInfoForPowerCellProperty(bpy.types.PropertyGroup):
	def update(cls, context):
		context.scene.last_cell_props.copy(cls)
		context.scene.last_buzzer_props.copy(cls) # UX

	game_task: bpy.props.IntProperty(
		name="Game Task",
		description="Which game task handles this power cell?",
		min=0,
		update=update)
	movie_pos: bpy.props.PointerProperty(type=MoviePosProperty,update=update)
	
	def copy(cls, copy_from):
		cls["game_task"] = copy_from.game_task
		cls.movie_pos.copy(copy_from.movie_pos)

class LevelsLoadedProperty(bpy.types.PropertyGroup):
	lev0: bpy.props.EnumProperty(
		items=levels,
		name="Level A",
		description="First level to load")
	lev1: bpy.props.EnumProperty(
		items=levels,
		name="Level B",
		description="Second level to load")
	# TODO: Display a warning when setting one level as both,
	#       since that's very irresponsible >:(

class LoadBoundaryCmdProperty(bpy.types.PropertyGroup):
	cmd: bpy.props.EnumProperty(
		items=load_boundary_cmds,
		name="Command",
		description="Command to send when player goes through the load boundary")
	arg0: bpy.props.EnumProperty( # Let's hope we can alter the settings here later, after the command is chosen or changed
		items=[],
		name="Argument 0",
		description="The first argument")
	arg1: bpy.props.EnumProperty( # same here
		items=[],
		name="Argument 1",
		description="The second argument")

# Actual layouts

class EcoVentProperties(bpy.types.PropertyGroup):
	def update(cls, context):
		context.scene.last_vent_props.copy(cls)

	kind: bpy.props.EnumProperty(
		items=eco_type[1:],
		name="Type",
		description="What kind of eco?",
		update=update)
	blocker: bpy.props.EnumProperty(
		items=[("none", "None", "")], # Let's hope we can mutate this
		name="Blocker",
		description="Vent is blocked until this actor is dead or destroyed",
		update=update)
	# Amount forced to 1 (doesn't really matter)
	
	def copy(cls, copy_from):
		cls["kind"] = copy_from.kind
		cls["blocker"] = copy_from.blocker

class OrbCacheProperties(bpy.types.PropertyGroup):
	def update(cls, context):
		context.scene.last_orb_cache_props.copy(cls)
	
	orb_count: bpy.props.IntProperty(
		name="Orb Count",
		description="How many orbs are in this orb cache?",
		min=0,
		update=update)
	
	def copy(cls, copy_from):
		cls["orb_count"] = copy_from.orb_count

class ContainerProperties(bpy.types.PropertyGroup):
	def update(cls, context):
		context.scene.last_crate_props.copy(cls)

	container_type: bpy.props.EnumProperty(
		items=containers,
		name="Type",
		description="Type of the container",
		update=update)
		#set=TODO: Change mesh when changing container_type)
	container_item: bpy.props.EnumProperty(
		items=containable,
		name="Item",
		description="What does this container contain?",
		update=update)

	# Only one of these is shown at once
	eco_info_eco: bpy.props.PointerProperty(type=EcoInfoForEcoProperty,update=update)
	eco_info_money: bpy.props.PointerProperty(type=EcoInfoForMoneyProperty,update=update)
	eco_info_buzzer: bpy.props.PointerProperty(type=EcoInfoForScoutFlyProperty,update=update)
	eco_info_cell: bpy.props.PointerProperty(type=EcoInfoForPowerCellProperty,update=update)

	instant_collect: bpy.props.BoolProperty(
		name="Collect Instantly",
		description="Make the player collect the item immediately after dropping it",
		default=False,
		update=update)
	indestructible: bpy.props.BoolProperty(
		name="Indestructible",
		description="Make the player unable to destroy the crate",
		update=update)
	
	def copy(cls, copy_from):
		cls["container_type"] = copy_from.get("container_type", "Wooden")
		cls["container_item"] = copy_from.get("container_item", "Eco")
		
		cls.eco_info_eco.copy(copy_from.eco_info_eco)
		cls.eco_info_money.copy(copy_from.eco_info_money)
		cls.eco_info_buzzer.copy(copy_from.eco_info_buzzer)
		cls.eco_info_cell.copy(copy_from.eco_info_cell)

		cls["instant_collect"] = copy_from.instant_collect
		cls["indestructible"] = copy_from.indestructible

class CheckpointProperties(bpy.types.PropertyGroup):
	name: bpy.props.StringProperty(
		name="Checkpoint Name",
		description="The name of this checkpoint",
		default="", # Validate this isn't empty on export
		maxlen=1024)
	level: bpy.props.PointerProperty(type=LevelsLoadedProperty)
	# TODO: Put load commands under advanced options

class LoadBoundaryProperties(bpy.types.PropertyGroup):
	closed: bpy.props.BoolProperty(
		name="Horizontal",
		description="Is the checkpoint trigger a horizontal plane",
		default=False)
	player: bpy.props.BoolProperty(
		name="Need Player",
		description="Activate only when player crossed the checkpoint trigger, not camera",
		default=True)
	# TODO: Handle :top and :bot automatically on export
	fwd: bpy.props.PointerProperty(type=LoadBoundaryCmdProperty)
	bwd: bpy.props.PointerProperty(type=LoadBoundaryCmdProperty)


# Eve's note: Why here???
classes = [  # Initialize the class array to be registered
	# Singular properties
	MoviePosProperty, LauncherVectorProperty,
	EcoInfoForEcoProperty, EcoInfoForMoneyProperty,
	EcoInfoForScoutFlyProperty, EcoInfoForPowerCellProperty,
	LevelsLoadedProperty, LoadBoundaryCmdProperty,

	# Layouts
	# Collectables
	EcoVentProperties,
	# Containers
	OrbCacheProperties, ContainerProperties,
	# Misc
	CheckpointProperties, LoadBoundaryProperties]


class OBJECT_PT_ActorInfoMenu(bpy.types.Panel):
	""""""

	bl_idname = "OBJECT_PT_ActorInfoMenu"
	bl_label = "Actor Info"
	bl_space_type = "VIEW_3D"
	bl_region_type = "UI"
	bl_category = "Level Editor"
	bl_context = "objectmode"

	@classmethod
	def poll(cls, context):
		# Only draw if is actor
		return (context.active_object is not None
				and context.active_object.select_get()
				and "Actor Type" in context.active_object.keys())

	def draw(self, context):
		layout = self.layout
		scene = context.scene
		obj = context.active_object

		def custom_prop(name, parent=layout):
			if bpy.context.object.get(name) is not None:
				parent.prop(bpy.context.object,
					f'["{name}"]')

		layout.prop(context.active_object, "name", text="Actor Name")

		etype = layout.row()
		etype.enabled = False

		custom_prop("Actor Type", etype)

		layout.separator()

		prop_data = None
		def add_props(props, data=None):
			if data is None: # lol
				data = prop_data
			if data is not None:
				for prop in props:
					layout.prop(data, prop)
		
		def add_movie_pos(data=None):
			data = prop_data if data is None else data
			if data is not None:
				add_props(["pos", "rot"], data)

		# Show all of the custom properties
		match obj.get("Actor Type"):
			case "buzzer":
				prop_data = obj.buzzer_props # Different
				layout.prop(prop_data, "game_task")
				add_movie_pos(prop_data.movie_pos)
			
			case "fuel-cell": # Could merge with buzzer using an if
				prop_data = obj.cell_props # Different
				layout.prop(prop_data, "game_task")
				add_movie_pos(prop_data.movie_pos)
			
			case "eco":
				prop_data = obj.eco_props
				layout.props_enum(prop_data, "kind")
				layout.prop(prop_data, "amount")
			
			case "ecovent":
				prop_data = obj.vent_props
				layout.props_enum(prop_data, "kind")
				layout.prop(prop_data, "blocker")
			
			case "orb-cache-top":
				prop_data = obj.orb_cache_props
				layout.prop(prop_data, "orb_count")

			case "crate":
				prop_data = obj.crate_props
				add_props(["container_type", "container_item"])

				match prop_data.container_item:
					case "eco":
						eco_data = prop_data.eco_info_eco
						layout.props_enum(eco_data, "kind") # coolness +2.5
						layout.prop(eco_data, "amount")
					case "money":
						money_data = prop_data.eco_info_money
						layout.prop(money_data, "amount")
					case "buzzer":
						buzzer_data = prop_data.eco_info_buzzer
						layout.prop(buzzer_data, "game_task")
						add_movie_pos(buzzer_data.movie_pos)
					case "fuel_cell":
						cell_data = prop_data.eco_info_cell
						layout.prop(cell_data, "game_task")
						add_movie_pos(cell_data.movie_pos)

				add_props(["instant_collect", "indestructible"])

		layout.separator()


classes.append(OBJECT_PT_ActorInfoMenu)  # Add the class to the array


##############################################################################
# Functions
##############################################################################

##############################################################################
# Registration
##############################################################################


def register():
	for cls in classes:  # Register all the classes
		bpy.utils.register_class(cls)
	
	# This is pain. There's no better way to do it I could figure out.
	bpy.types.Object.buzzer_props = bpy.props.PointerProperty(type=EcoInfoForScoutFlyProperty)
	bpy.types.Object.cell_props = bpy.props.PointerProperty(type=EcoInfoForPowerCellProperty)
	bpy.types.Object.eco_props = bpy.props.PointerProperty(type=EcoInfoForEcoProperty)
	bpy.types.Object.vent_props = bpy.props.PointerProperty(type=EcoVentProperties)
	bpy.types.Object.orb_cache_props = bpy.props.PointerProperty(type=OrbCacheProperties)
	bpy.types.Object.crate_props = bpy.props.PointerProperty(type=ContainerProperties)

	# Double that for the Scene type cause UX go brrrr
	bpy.types.Scene.last_buzzer_props = bpy.props.PointerProperty(type=EcoInfoForScoutFlyProperty)
	bpy.types.Scene.last_cell_props = bpy.props.PointerProperty(type=EcoInfoForPowerCellProperty)
	bpy.types.Scene.last_eco_props = bpy.props.PointerProperty(type=EcoInfoForEcoProperty)
	bpy.types.Scene.last_vent_props = bpy.props.PointerProperty(type=EcoVentProperties)
	bpy.types.Scene.last_orb_cache_props = bpy.props.PointerProperty(type=OrbCacheProperties)
	bpy.types.Scene.last_crate_props = bpy.props.PointerProperty(type=ContainerProperties)

def unregister():
	for cls in reversed(classes):  # Unregister all the classes
		bpy.utils.unregister_class(cls)
	
	# Repeat pain
	del bpy.types.Object.buzzer_props
	del bpy.types.Object.cell_props
	del bpy.types.Object.eco_props
	del bpy.types.Object.vent_props
	del bpy.types.Object.orb_cache_props
	del bpy.types.Object.crate_props

	# Twice
	del bpy.types.Scene.last_buzzer_props
	del bpy.types.Scene.last_cell_props
	del bpy.types.Scene.last_eco_props
	del bpy.types.Scene.last_vent_props
	del bpy.types.Scene.last_orb_cache_props
	del bpy.types.Scene.last_crate_props

# if __name__ == "__main__":  # For internal Blender script testing
#    register()
