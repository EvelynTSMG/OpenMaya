##############################################################################
# Imports           Order: 3rd Party Imports, Python Built-ins, Custom Modules
##############################################################################


import bpy
import typing
import re
import os
import json

from . import export


##############################################################################
# Classes
##############################################################################


classes = []  # Initialize the class array to be registered


class LevelProperties(bpy.types.PropertyGroup):
	custom_levels_path: bpy.props.StringProperty(
		name="Working Directory",
		description="The path to \\custom_levels\\ in the OpenGOAL distribution",
		default="",
		maxlen=1024,
		subtype="DIR_PATH")
	
	should_export_level_info: bpy.props.BoolProperty(
		name="Level Info",
		description="Check if you'd like the level info to be included when you export",
		default=True)
	should_export_actor_info: bpy.props.BoolProperty(
		name="Actor Info",
		description="Check if you'd like the actor info to be included when you export",
		default=True)
	should_export_geometry: bpy.props.BoolProperty(
		name="Level Geometry",
		description="Check if you'd like the level geometry to be included when you export",
		default=True)
	should_playtest_level: bpy.props.BoolProperty(
		name="Playtest Level",
		description="Check if you'd like to launch the level immediately after export",
		default=True)
	
	automatic_wall_detection: bpy.props.BoolProperty(
		name="Auto Wall Detection",
		description="Check if you'd like the level to automatically create walls above a certain angle",
		default=True)
	automatic_wall_angle: bpy.props.FloatProperty(
		name="Auto Wall Angle",
		description="The angle you'd like the level to automatically create walls above",
		min=0.0,
		max=90.0,
		default=45.0)
	double_sided_collide: bpy.props.BoolProperty(
		name="Double Sided Collision",
		description="Check if you'd like to make all collision mesh triangles double sided",
		default=False)

	index: bpy.props.IntProperty(
		name="Level Index",
		description="The index of your custom level",
		min=0,
		default=17)
	title: bpy.props.StringProperty(
		name="Level Title",
		description="The name of your custom level.\nOnly letters and dashes are allowed, case will be ignored.\nDefault: my-level",
		default="my-level",
		maxlen=1024)
	# visname = title + "-vis"
	nickname: bpy.props.StringProperty(
		name="Level Nickname",
		description="The nickname of your custom level.\nThree letters, case will be ignored.\nDefault: lvl",
		default="lvl",
		maxlen=3)
	# TODO: Do packages automatically (just do whatever the level name is it doesn't matter)

	music_bank: bpy.props.EnumProperty(
		items=[], # TODO: Populate when loading addon
		name="Music Bank",
		description="The music bank of your level")
	ambient: bpy.props.EnumProperty(
		items=[], # TODO: Populate when loading addon
		name="Ambient Sounds",
		description="The bank of ambient sounds for your level")
	mood: bpy.props.EnumProperty(
		items=[], # TODO: Populate when loading addon
		name="Mood",
		description="The atmosphere (mood) of your level")
	mood_func: bpy.props.EnumProperty(
		items=[], # TODO: Populate when loading addon
		name="Mood Function",
		description="The function responsible for dynamically changing to the mood of your level")

	ocean: bpy.props.EnumProperty(
		items=[], # TODO: Populate when loading addon
		name="Ocean Map",
		description="What ocean is visible in the level?(?)")
	sky: bpy.props.BoolProperty(
		name="Has Sky",
		description="Is the sky visible in your level?",
		default=True)
	sun_fade: bpy.props.FloatProperty(
		name="Sun Fade",
		description="???",
		min=0,max=1)

	# Priority = 200 if true, 100 otherwise
	is_hub: bpy.props.BoolProperty(
		name="Is Hub",
		description="Is your level a hub level? Affects priority",
		default=False)
	
	bottom_height: bpy.props.FloatProperty(
		name="Bottom Height",
		description="Height of the bottom kill plane of the level",
		default=-8000) # safe default
	orb_count: bpy.props.IntProperty( # For use with Zed's custom-orb-count fix
		name="Orb Count",
		description="Total amount of orbs in your level",
		min=0)
	buzzer_count: bpy.props.IntProperty( # For use with Zed's custom-buzzer-count fix
		name="Scout Fly Count",
		description="Total amount of scout flies in your level",
		min=0)

classes.append(LevelProperties)  # Add the class to the array


class OBJECT_PT_LevelInfoMenu(bpy.types.Panel):
	""""""

	bl_idname = "OBJECT_PT_LevelInfoMenu"
	bl_label = "Level Info"
	bl_space_type = "VIEW_3D"
	bl_region_type = "UI"
	bl_category = "Level Editor"
	bl_context = "objectmode"

	def draw(self, context):

		layout = self.layout
		scene = context.scene
		level_properties = scene.level_properties

		def input_invalid(chars, value):
			return not (bool(re.match(chars, value)) and value)

		# set these properties manually
		title = layout.row()
		title.alert = input_invalid("^[A-Za-z-]*$", level_properties.title)

		title.prop(level_properties, "title", icon="TEXT")

		nick = layout.row()
		nick.alert = input_invalid("^[A-Za-z]*$", level_properties.nickname)
		nick.prop(level_properties, "nickname", icon="TEXT")

		# layout.operator("wm.create_world_reference")

		layout.prop(level_properties, "custom_levels_path")

		layout.prop(level_properties, "should_export_level_info")

		layout.prop(level_properties, "should_export_actor_info")

		layout.prop(level_properties, "should_export_geometry")

		layout.prop(level_properties, "should_playtest_level")

		layout.operator("wm.export")

		layout.separator()

		layout.prop(level_properties, "automatic_wall_detection")

		layout.prop(level_properties, "automatic_wall_angle")

		layout.prop(level_properties, "double_sided_collide")


classes.append(OBJECT_PT_LevelInfoMenu)  # Add the class to the array


##############################################################################
# Functions
##############################################################################


# Need to implement a singleton pattern so this only shows once per error
def show_message(message, title="Message", icon="INFO"):
	"""Make a little popup"""

	def draw(self, context):
		self.layout.label(text=message)

	bpy.context.window_manager.popup_menu(draw, title=title, icon=icon)


##############################################################################
# Registration
##############################################################################


def register():

	for cls in classes:  # Register all the classes
		bpy.utils.register_class(cls)

	bpy.types.Scene.level_properties = bpy.props.PointerProperty(type=LevelProperties)


def unregister():

	for cls in reversed(classes):  # Unregister all the classes
		bpy.utils.unregister_class(cls)

	del bpy.types.Scene.level_properties


# if __name__ == "__main__":  # For internal Blender script testing
#    register()
